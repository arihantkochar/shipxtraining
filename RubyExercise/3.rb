def prime(n)
    sqr=Integer(Math.sqrt(n))
    k=2
    while k<=sqr
       
        if n%k==0
            return false
        end
        k=k+1
    end
    return true
end

b=Integer(600851475143/2)
puts b
while b>=2
    if prime(b)==true
        if 600851475143%b==0
            puts "Largest prime factor : #{b}"
            break
        end
    end
    b-=1
end 
