multiples_of_3_5 = Proc.new do |n|
  n % 3 == 0 && n % 5 == 0
end

print (1..1000).to_a.select(&multiples_of_3_5)
